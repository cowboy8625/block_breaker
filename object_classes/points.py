import object_classes as oc


class Point:
    __slots__ = ["x", "y"]

    def __init__(self, x, y):
        self.x = int(x)
        self.y = int(y)

    def __repr__(self):
        return f"Point({self.x}, {self.y})"

    def __add__(self, other):
        if isinstance(other, Point):
            return Point(int(self.x + other.x), int(self.y + other.y))
        if isinstance(other, (int, float)):
            return Point(int(self.x + other), int(self.y + other))

    def __sub__(self, other):
        if isinstance(other, Point):
            return Point(int(self.x - other.x), int(self.y - other.y))
        if isinstance(other, (int, float)):
            return Point(int(self.x - other), int(self.y - other))

    def __truediv__(self, other):
        if isinstance(other, Point):
            return Point(int(self.x / other.x), int(self.y / other.y))
        if isinstance(other, (int, float)):
            return Point(int(self.x / other), int(self.y / other))

    def __mul__(self, other):
        if isinstance(other, Point):
            return Point(int(self.x * other.x), int(self.y * other.y))
        if isinstance(other, (int, float)):
            return Point(int(self.x * other), int(self.y * other))

    def __eq__(self, other):
        if isinstance(other, Point):
            return True if self.x == other.x and self.y == other.y else False

    def __hash__(self):
        return hash((self.x, self.y))
