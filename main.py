from tkinter import Tk, Frame, Canvas
from random import randint

from object_classes import Ball, Point

SPAWNX = SPAWNY = 0
BACKGROUND = "black"
GRID = 32
WIDTH = GRID * 32
HEIGHT = GRID * 32


def board_to_canvas(x, y):
    return x * GRID, y * GRID


def ran_color():
    return "#" + ("%06x" % randint(0, 16777215))


class App:
    def __init__(self, root):
        self.root = root
        self.initUI()
        self.var()
        self.update()

    def initUI(self):
        self.root.geometry(f"{WIDTH}x{HEIGHT}+{SPAWNX}+{SPAWNY}")
        self.root.title("BALL BOUNCE")
        self.canvas = Canvas(width=WIDTH, height=HEIGHT, bg=BACKGROUND)
        self.canvas.pack()

    def var(self):
        self.VEL = 8
        self.balls = []
        self.blocks = []
        self.spawn_ball(n=6)

    def spawn_ball(self, n=1):

        for num in range(n):
            x = randint(1, round(WIDTH / GRID))
            y = randint(1, round(HEIGHT / GRID))
            x, y = board_to_canvas(x, y)
            ball = Ball(self.canvas, x, y, GRID / 2, ran_color(), f"ball{num}")
            ball.constrain(WIDTH, HEIGHT, 0, 0)

            self.balls.append(ball)

    def update(self):
        for ball in self.balls:
            ball.update()
        self.root.after(10, self.update)


if __name__ == "__main__":
    root = Tk()
    app = App(root)
    root.mainloop()

